package com.example.yshakori.myapplication;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.downloader.Progress;
import com.gauravk.audiovisualizer.visualizer.BlastVisualizer;
import com.gauravk.audiovisualizer.visualizer.CircleLineVisualizer;
import com.google.gson.Gson;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.yshakori.myapplication.UrlConstants.GET_URL_REQUEST_CODE;
import static com.example.yshakori.myapplication.UrlConstants.Get_Dolar_REQUEST_CODE;
import static com.example.yshakori.myapplication.UrlConstants.Get_F2Pool_REQUEST_CODE;
import static com.example.yshakori.myapplication.UrlConstants.POST_URL_REQUEST_CODE;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, NetworkController.ResultListener{

    Button getButton;
    Button postButton;
    Button btnF2Pool;
    TextView responseTv;
    Button btnDownload;
    MediaPlayer mAudioPlayer;
    //BlastVisualizer mVisualizer;
    CircleLineVisualizer mVisualizer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getButton = (Button) findViewById(R.id.getButton);
        postButton = (Button) findViewById(R.id.postButton);
        responseTv = (TextView) findViewById(R.id.response);
        btnF2Pool=(Button)findViewById(R.id.btnGetF2Pool);
        btnDownload=(Button)findViewById(R.id.btnDownload);

        getButton.setOnClickListener(this);
        postButton.setOnClickListener(this);
        btnF2Pool.setOnClickListener(this);
        btnDownload.setOnClickListener(this);




        initConfig();


        mAudioPlayer = MediaPlayer.create(this, R.raw.mu);
        mAudioPlayer.start();

        //get reference to visualizer
        mVisualizer =(CircleLineVisualizer) findViewById(R.id.blast);

        //TODO: init MediaPlayer and play the audio

        //get the AudioSessionId from your MediaPlayer and pass it to the visualizer
        int audioSessionId = mAudioPlayer.getAudioSessionId();
        if (audioSessionId != -1)
            try {
                mVisualizer.setAudioSessionId(audioSessionId);
            }
            catch (Exception ex)
            {
                Log.e("Tag","Error: "+ex);
            }

        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        Uri  videoURI = Uri.parse("android.resource://" + getPackageName() +"/"
                +R.raw.mu);
        File file=new File("raw\\mu.mp3");
        mmr.setDataSource(this,videoURI);
        byte [] data = mmr.getEmbeddedPicture();
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
       // ImageView imageView=(ImageView)findViewById(R.id.imageView);
       // imageView.setImageBitmap(bitmap);


        CircleImageView imageView2=(CircleImageView)findViewById(R.id.profile_image);
        imageView2.setImageBitmap(bitmap);

        imageView2.setOnClickListener(this);


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVisualizer != null) {
            mVisualizer.release();
            mAudioPlayer.release();
            if (mVisualizer != null)
                mVisualizer.hide();
        }
    }
    @Override
    public void onClick(View v) {
        Log.d("TAG",String.valueOf(v.getId()));
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
        switch (v.getId())
        {
            case R.id.getButton:

                NetworkController.getInstance().connect(this, GET_URL_REQUEST_CODE, Request.Method.GET, null, this);

                break;
            case R.id.postButton:

                HashMap<String, String> stringParams = new HashMap<>();
                stringParams.put("name", "Vishal");

                NetworkController.getInstance().connect(this, Get_Dolar_REQUEST_CODE, Request.Method.GET, null, this);

                break;
            case R.id.btnGetF2Pool:

                String resPath="";
            {
                String st = "/storage";
                File sa = new File(st);
                final File[] fa = sa.listFiles();
                for (int j = 0; j < fa.length; j++) {
                    try {
                        if (fa[j].getName().contains("self") || fa[j].getName().contains("emulated")) {
                            continue;
                        }
                        else {
                            resPath = "/storage/" + fa[j].getName();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }


                //Find the directory for the SD Card using the API
//*Don't* hardcode "/sdcard"
                File sdcard = Environment.getExternalStorageDirectory();

//Get the text file
                File file = new File(resPath,"config2.txt");

//Read text from file
                StringBuilder text = new StringBuilder();

                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String line;

                    //read line by line
                    while ((line = br.readLine()) != null) {
                        text.append(line);
                        text.append('\n');
                    }
                }
                catch (IOException e) {
                    //You'll need to add proper error handling here
                    Log.d("Tag",e.getMessage());
                }

                NetworkController.getInstance().connect(this, Get_F2Pool_REQUEST_CODE, Request.Method.GET, null, this);

                break;
            case  R.id.btnDownload:
            Log.e("TAG","Start Download");
            Log.d("TAG","Start Download");

                String resPath2="";
            {
                String st = "/storage";
                File sa = new File(st);
                final File[] fa = sa.listFiles();
                for (int j = 0; j < fa.length; j++) {
                    try {
                        if (fa[j].getName().contains("self") || fa[j].getName().contains("emulated")) {
                            continue;
                        }
                        else {
                            resPath2 = "/storage/" + fa[j].getName();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }


            Download("http://dl3.soft98.ir/win/Microsoft.Windows.7.SP1.Ultimate.January.2019.x86.part1.rar", resPath2,"Part1.rar");

            break;
            case R.id.profile_image:
                if(!mAudioPlayer.isPlaying())
                    mAudioPlayer.start();
                else
                    mAudioPlayer.pause();
                break;

        }
    }
    public  void Download(String url,String dirPath,String fileName)
    {
        int downloadId = PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        Log.e("TAG",progress.toString()+"->"+String.valueOf(progress.currentBytes/progress.totalBytes*100)+"%");
                        Toast.makeText(getApplicationContext(),String.valueOf(progress.currentBytes/progress.totalBytes*100),Toast.LENGTH_SHORT);
                        double test=((double) 50.0/(double) 180.0);
                        String dfg=String.format("Value of a: %.2f", test);
                        double perc=(double) progress.currentBytes/(double) progress.totalBytes*100.0;
                        responseTv.setText(String.format("%.2f",perc)+"%");
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {

                    }

                    @Override
                    public void onError(Error error) {
                        //Log.e("TAG",error.getServerErrorMessage());

                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                1);

                    }
                });
    }
    public  void initConfig(){
        Log.e("TAG","Start Init COnfig");
        // Enabling database for resume support even after the application is killed:
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onResult(int requestCode, boolean isSuccess, JSONObject jsonObject, VolleyError volleyError, ProgressDialog progressDialog) {

        if (requestCode == POST_URL_REQUEST_CODE)
        {
            if (isSuccess)
            {
                Log.e("MainActivity", "onResult() called 2");
                responseTv.setText(jsonObject.toString());
            }
            else
            {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        }
        else if (requestCode == GET_URL_REQUEST_CODE)
        {
            if (isSuccess)
            {
                Log.e("MainActivity", "onResult() called 1");
                responseTv.setText(jsonObject.toString());
                Gson gsonConvertor=new Gson();
                RootObject response= gsonConvertor.fromJson(jsonObject.toString(),RootObject.class);
                Toast.makeText(getApplicationContext(), response.bpi.USD.rate, Toast.LENGTH_LONG).show();

            }
        }
        else if (requestCode == Get_Dolar_REQUEST_CODE)
        {
            if (isSuccess)
            {
                Log.e("MainActivity", "onResult() called 1");
                responseTv.setText(jsonObject.toString());
                Gson gsonConvertor=new Gson();
                RootObjectDolar response= gsonConvertor.fromJson(jsonObject.toString(),RootObjectDolar.class);
                Toast.makeText(getApplicationContext(), response.buy_usd.price, Toast.LENGTH_LONG).show();

            }
        }
        else if (requestCode == Get_F2Pool_REQUEST_CODE)
        {
            if (isSuccess)
            {
                Log.e("MainActivity", "onResult() called 1");
                responseTv.setText(jsonObject.toString());
                Gson gsonConvertor=new Gson();
                F2PoolModel response= gsonConvertor.fromJson(jsonObject.toString(),F2PoolModel.class);
                Toast.makeText(getApplicationContext(),String.valueOf(response.balance), Toast.LENGTH_LONG).show();

            }
        }
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}