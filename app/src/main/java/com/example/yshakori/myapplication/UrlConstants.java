package com.example.yshakori.myapplication;

public interface UrlConstants {

    String GET_URL = "https://api.coindesk.com/v1/bpi/currentprice/USD.json";
    String POST_URL = "http://androiderstack.com/api/postAPI.php";
    String Get_Dolar="http://www.megaweb.ir/api/money";
    String Get_F2Pool="http://api.f2pool.com/bitcoin/yshakoury";


    int GET_URL_REQUEST_CODE = 1;
    int POST_URL_REQUEST_CODE = 2;
    int Get_Dolar_REQUEST_CODE = 3;
    int Get_F2Pool_REQUEST_CODE = 4;
}