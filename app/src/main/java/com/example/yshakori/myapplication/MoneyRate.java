package com.example.yshakori.myapplication;

/**
 * Created by y.shakori on 6/23/2019.
 */

public class MoneyRate {
    public class BuyUsd
    {
        public String title ;
        public String price ;
        public String jdate ;
        public String gdate ;
    }

    public class BuyEur
    {
        public String title ;
        public String price ;
        public String jdate ;
        public String gdate ;
    }

    public class BuyAed
    {
        public String title ;
        public String price ;
        public String jdate ;
        public String gdate ;
    }

    public class SellUsd
    {
        public String title ;
        public String price ;
        public String jdate ;
        public String gdate ;
    }

    public class SellEur
    {
        public String title ;
        public String price ;
        public String jdate ;
        public String gdate ;
    }

    public class SellAed
    {
        public String title ;
        public String price ;
        public String jdate ;
        public String gdate ;
    }

    public class RootObjectDolar
    {
        public BuyUsd buy_usd ;
    }
}

